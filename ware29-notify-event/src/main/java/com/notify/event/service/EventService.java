package com.notify.event.service;

import com.notify.event.entity.OrderState;
import com.notify.event.entity.OrderStateEvent;
import com.notify.event.listener.OrderStateListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;

/**
 * @author 公众号:知了一笑
 * @since 2022-04-23 20:06
 */
@Service
public class EventService implements ApplicationContextAware, ApplicationEventPublisherAware {

    private static final Logger logger = LoggerFactory.getLogger(EventService.class) ;

    private ApplicationContext applicationContext;
    private ApplicationEventPublisher applicationEventPublisher;

    public void changeState (Integer orderId,Integer stateFrom,Integer stateTo){

        OrderState orderState = new OrderState() ;
        orderState.setEventId(2022042301);
        orderState.setCreateTime(System.currentTimeMillis());
        orderState.setVersion("master");

        orderState.setSource("order-server");
        orderState.setTarget("other-server");

        orderState.setOrderId(orderId);
        orderState.setStateFrom(stateFrom);
        orderState.setStateTo(stateTo);

        OrderStateEvent orderStateEvent = new OrderStateEvent(orderState) ;
        logger.info(Thread.currentThread().getName()+";"+orderStateEvent);
        applicationEventPublisher.publishEvent(orderStateEvent);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext ;
    }
    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher ;
    }
}
